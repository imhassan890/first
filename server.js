const express =  require('express')
const app = express()
const dotenv = require('dotenv')

dotenv.config();

app.get('/healthcheck',(req,res)=>{
res.status(500).send('Okay from Server')

})

const posts=[{post_id:'1',title:'This is a title 1',description:'This is description 1',created_at:'10 june 2018'},
            {post_id:'2',title:'This is a title 2',description:'This is description 2',created_at:'20 july 2019'},
            {post_id:'3',title:'This is a title 3',description:'This is description 3',created_at:'30 August 2020'}
]

app.get('/post',(req,res)=>{
  res.send(posts)
    
    })


app.listen(process.env.PORT)

